/* ====== DEFAULT SCRIPTS ====== */


/* ======
DEFAULT SCRIPTS
  - BASE JS
  - WINDOW LOAD
  - WINDOW RESIZE
  - ON SCROLL
  - INSURANCE LOOKUP
  - ENCODE HTML
====== */



(function(){
  
	var $ = jQuery.noConflict();
  // var textSections = {};
  // var count = 0;

  // $( "section" ).each(function() {
  //   var $this = $(this); 
  //   count ++;
   
  //   $( $this ).attr('data-section-id', count);
  //   var thisId = count;
    
  //   //Build array of the section name name and ID
  //   if( thisId > 0 ){
  //     textSections[thisId] = thisId;
  //   }    

  //});

  //console.log( textSections );
  /*================================================================================
  $ BASE JS
  ================================================================================*/
  
  //Detect search inpout changes
  $('#search_input').on('input', function() {
    var $this = $(this);
    var term = $this.val();
    term = encodeHTML( term.toLowerCase() );
    insuranceLookUp( term );      
  });

  //Clicking submit run search
  $( ".search-icon" ).on( "click", function(e) {
    e.preventDefault();
    var term = $('#search_input').val();
    term = encodeHTML( term.toLowerCase() );
    insuranceLookUp( term );  
  });

  //Products details Tabbed NAV
  $( ".product-details-nav li" ).on( "click", function() {
    var details = $(this).attr('id');
    $( ".product-details-nav li" ).removeClass('active');
    $(this).toggleClass('active');

    $('.product-details .tabbed-content').removeClass('active');
    $('.'+details).toggleClass('active');
     
  });

  //Hide show products points
  $( ".cover-pnts .tick-icon" ).on( "click", function(e) {
    e.preventDefault();
    var thisDetails = $(this).find('.cover-pnts__details');
    thisDetails.toggleClass('active');
    $( ".cover-pnts .tick-icon .cover-pnts__details").not(thisDetails).removeClass('active');
  });

  //Hide show products points
  $( ".accordion" ).on( "click", function() {    
    $( this ).toggleClass('active').not(this).removeClass('active');
  });

  //Hide show menu
  $( ".more-menu" ).on( "click", function() {   
    buildOverlayMenu(); 
    $( '.overlay' ).toggleClass('active');
  });

  $( "#close" ).on( "click", function() { 
  //$("body").on("click","#close", function(){ 
    $( '.overlay' ).toggleClass('active');
    //alert('click');
  });

  


  // $( "body" ).on( "click", function() {
  //   $('#search_results').html('');
  // });

  /*================================================================================
  $ WINDOW LOAD
  ================================================================================*/

	$(window).load(function() {

  });


  /*================================================================================
  $ WINDOW RESIZE
  ================================================================================*/ 

  $(window).resize(function() {

  });


  /*================================================================================
  $ ON SCROLL
  ================================================================================*/ 

  $(window).on('scroll', function() {
    /*================================================================================
      $ ADD VISIBILTY TO SECTION WHEN IN VIEW
    ================================================================================*/

    // if(textSections){

    //   $.each(textSections, function (index, value) {
        
    //     var sectionDataId = index;
    //     console.log(sectionDataId);
    //     // var sectionClass = '.'+index;

    //     // if( $(sectionId).is(":visible") ){
          
    //     //   if(isScrolledIntoView(sectionId)){

    //     //     $( 'section'+sectionId ).addClass('visible');
            
    //     //   }  

    //     // }

    //   }); 
    // }

  });
     
    
  /*================================================================================
  $ INSURANCE LOOKUP
  ================================================================================*/ 


  function insuranceLookUp( term ){

    var results = [];
    var resultsHtml = '';
    var countResults = 0;

// Commercial Property – Commercial Affinities
// Blocks of flats - Deacon
// Recruitment agency – Commercial Affinities
// Tradesman – Commercial Affinities
// Dance Instructor – Commercial Affinities
// Critical Illness - ????
// Life Assurance - ?????
// Landlord - Deacon
// Tenant - Intasure
// Car – Wakefield 
// Travel – George Burrows or Covermore (Affinity partner) ????
// Impaired Travel – charity solutions
// Thatched – Commercial Affinities
// Listed Home - - Intasure
// High Net Worth Insurance – Private Clients
// Over 50’s Home Insurance – Insure 4 Retirement
// UK Holiday Home - Intasure
// Holiday Home Abroad - Intasure
// Static Caravan - HAHP
// Park Home - HAHP
// Yacht – Intasure

     var insuranceproducts = [
        { "iproduct":"Business","category":"Business", "url":"business.html", "keywords":[ "commercial", "buildings"] },
        { "iproduct":"Personal", "url":"personal.html", "keywords":[ "health", "life", 'will'] },     
        { "iproduct":"Life Insurance", "url":"life.html", "keywords":[ "health", "life", 'will'] }        
    ]; 

    if ( term.length > 0 ) {
      resultsHtml += '<ul>';
      // Loop through each product
      for(var i = 0; i < insuranceproducts.length; i++) {

        // loop through each keywood for each product
        for(var k = 0; k < insuranceproducts[i].keywords.length; k++) {

          //If we find a match for the search term display it
          if( insuranceproducts[i].keywords[k].indexOf(term) > -1 ){

            //console.log( insuranceproducts[i].iproduct );

            //console.log('hello');

            countResults ++;
            var found = $.inArray( insuranceproducts[i].iproduct, results ) > -1;

            //If we dont have the product in the result we add it, this is to prevent duplication

            if( !found ){
              //Push product to results array
              results.push(insuranceproducts[i].iproduct);

              //Build HTML results
              resultsHtml += '<li>';
                resultsHtml += '<div class="search-details ' + insuranceproducts[i].iproduct.toLowerCase() + '-insurance">';
                  resultsHtml += '<a class="icon" href="' + insuranceproducts[i].url + '"><span><img src="images/icons/' + insuranceproducts[i].iproduct.toLowerCase() + '.svg" alt="' + insuranceproducts[i].iproduct + '"></span></a>';
                  resultsHtml += '<a href="' + insuranceproducts[i].url + '" class="result-btn">' + insuranceproducts[i].iproduct + '</a>';
                resultsHtml += '</div>';
              resultsHtml += '</li>';
            }
            
            
          }

        }
        
      }

      resultsHtml += '</ul>';

    };

    //console.log( results );

    if( countResults > 0 ){
      $('#search_results').html( resultsHtml );
      //alert( results );
    } else {
      $('#search_results').html( '<div class="no-results"><p>No results found</p></div>' );
    }
    
    

  }

   
  /*================================================================================
  $ ENCODE HTML
  ================================================================================*/
  function encodeHTML( string ) {
      return string.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/"/g, '&quot;');
  }

  /*================================================================================
  $ BUILD OVERLAY MENU
  ================================================================================*/
  function buildOverlayMenu( ) {

    if( $( ".overlay nav" ).length ){
      //Menu has already been generated
      return;
    }

    var menu = [];
    var count = 0;
    var menuHtml = '</div>';
        menuHtml += '<nav><ul>';

          $( "nav.js-om" ).each(function() {

            menuItems = $(this).find('li');

            $( menuItems ).each(function() {        
              menuName = $(this).find('a').text();
              menuLink = $(this).find('a').attr('href');

              menuHtml += '<li><a href="' + menuLink + '">' + menuName + '</a></li>';
             
              count ++;

            }); 

          });

        menuHtml += '<nav><ul>';

    if( count > 0 ){
      $( ".overlay__content" ).html( menuHtml );
    }

  }

  /*================================================================================
  $ GET SCROLLED INTO VIEW JS

  isScrolledIntoView('.section-content')

  ================================================================================*/
  function isScrolledIntoView(elementclass){
     
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elementclass).offset().top;
      var elemBottom = elemTop + $(elementclass).height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
  }


})(); 


